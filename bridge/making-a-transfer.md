# Making a Transfer

ChainFusion bridge allows to transfer assets between various blockchains and makes it fast, cheap, secure in a decentralized fashion.

## Transferring using UI

### Connect Wallet

Go to bridge app at [https://app.chainfusion.org](making-a-transfer.md#using-ui) and press **Connect Wallet** button.

<figure><img src="../.gitbook/assets/1 - Connect Wallet.png" alt=""><figcaption></figcaption></figure>

After pressing **Connect Wallet** you will see wallet selection dialog. Select one of the supported wallet providers.

<figure><img src="../.gitbook/assets/2 - Connect Wallet.png" alt=""><figcaption></figcaption></figure>

You will be prompted to connect wallet, press **Connect**. In this example we use [Metamask](https://metamask.io/).

<figure><img src="../.gitbook/assets/3 - Connect Wallet.png" alt="" width="375"><figcaption></figcaption></figure>

### Make a transfer

After you connect the wallet, you should select source chain, destination chain and enter amount. You can specify either transfer amount or receiving amount.

Optional: You can approve ERC-20 token spend by ChainFusion by pressing **Approve** button. Some token standards, like ERC-677 do not require approval.

<figure><img src="../.gitbook/assets/4 - Enter Amount.png" alt=""><figcaption></figcaption></figure>

Press **Transfer** button in order to initiate transfer of specified amount from **source chain** to **destination** chain.

<figure><img src="../.gitbook/assets/6 - Transfer.png" alt=""><figcaption><p>Transfer start</p></figcaption></figure>

<figure><img src="../.gitbook/assets/7 - Transfer Progress.png" alt=""><figcaption><p>Transfer progress</p></figcaption></figure>

<figure><img src="../.gitbook/assets/8 - Transfer Success.png" alt=""><figcaption><p>Transfer success</p></figcaption></figure>

### Recent transfers history

Recent transfers are shown in **Recent Transfers** section. You can select **All** in order to see all recent transfers or select **Only My** in order to see transfers performed by account you have connected.

Transfer history is limited only to recent transfers, older records won't be shown.

<figure><img src="../.gitbook/assets/9 - Recent Transfers.png" alt=""><figcaption></figcaption></figure>

## Transferring using bridge smart contract

To transfer assets from one blockchain to another, you would need to use [bridge contract's](https://gitlab.com/chainfusion/erc-20-bridge-contracts/-/blob/main/contracts/chain/ERC20Bridge.sol?ref_type=heads) `deposit()` function. Deposit function has four parameters:

* `token` - address of token to deposit, on the chain, you are transferring from
* `destinationChainId` - identifier of blockchain you are transferring to
* `receiver` - address of receiver, you are transferring to (usually the same account you are transferring from)
* `amount` - amount of tokens to transfer through bridge

After you made a deposit, usually it takes few minutes for validators to confirm your transfer and compose transaction on destination chain.


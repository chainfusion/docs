# Bridge Overview

<figure><img src=".gitbook/assets/LinkedIn - Cover 2.1 - with shadow.png" alt=""><figcaption></figcaption></figure>

ChainFusion is a decentralized bridge that provides fast, cheap and secure cross-chain communication, like sending assets between blockchains. We strongly believe that bridges should be decentralized and trustless, everyone should be able to become a validator and secure the bridge.

Recent events show that current cross-chain solutions are highly vulnerable, sometimes it's enough to compromise few private keys in order to steal all locked value. Risks increase with centralized setups, where only bridge operating company decides who could become a validator.

### Decentralizing the Bridge

On vast majority of the blockchains, to make transaction it's enough to create a valid ECDSA signature using private key. [Recent findings](https://eprint.iacr.org/2020/540) in cryptology propose a solution on how to make such signature by cooperation of any number of parties, without knowing private key. It also allows to identify parties that do not cooperate, effectively allowing to slash (ban) them from being validators using automated voting performed by nodes.

Using algorithm mentioned above, validators generate distributed key by performing several communication rounds between each other. After last round complete, each party derives his own secret key share. This key share allows honest majority of participants to create valid transaction signatures from mutual public key. This mutual public key has the rights to perform bridging actions, like sending assets on destination chain to receiver who deposited asset on source chain. To make this process happen, validators constantly monitor events on each blockchain supported by bridge and then perform signing rounds in cooperation with other validators. At the end of signing rounds, validators compose valid threshold ECDSA signature for transaction.

### Making bridging cheap

In bridges with setup of more than one validator, usually on-chain signature is used. It's costly to check signatures on-chain when number of validators increases, each new signature verification consumes transaction fee. More validators, means more signatures to verify, which means bigger on-chain confirmation costs, resulting in higher bridge fees.

Threshold ECDSA allow us to compose one single signature using majority of validators. It's much cheaper to validate single signature and it does not affect security of validation.

### Supported operations

Currently bridge supports ERC-20 token transfers, our team constantly expands list of supported blockchains and tokens. We plan to also support NFTs and arbitrary data transfer between blockchains soon.

## ChainFusion Testnet

Recently we have launched ChainFusion Testnet. Below you can find useful URLs to explore the project.

<table><thead><tr><th width="390">Name</th><th>URL</th></tr></thead><tbody><tr><td>Chain ID</td><td><code>953842</code></td></tr><tr><td>Explorer</td><td><a href="https://testnet-explorer.chainfusion.org/">https://testnet-explorer.chainfusion.org/</a></td></tr><tr><td>Bridge App</td><td><a href="https://app.chainfusion.org/">https://app.chainfusion.org/</a></td></tr><tr><td>HTTPS RPC API</td><td><a href="https://rpc.chainfusion.org">https://rpc.chainfusion.org</a></td></tr><tr><td>WebSocket RPC API</td><td><a href="wss://wc-rpc.chainfusion.org">wss://wc-rpc.chainfusion.org</a></td></tr></tbody></table>

## Bridgenet 1

<table><thead><tr><th width="390">Name</th><th>URL</th></tr></thead><tbody><tr><td>Chain ID</td><td><code>9991</code></td></tr><tr><td>Explorer</td><td><a href="https://explorer-bg1.chainfusion.org">https://explorer-bg1.chainfusion.org</a></td></tr><tr><td>HTTPS RPC API</td><td><a href="https://rpc-bg1.chainfusion.org">https://rpc-bg1.chainfusion.org</a></td></tr><tr><td>WebSocket RPC API</td><td><a href="wss://ws-rpc-bg1.chainfusion.org">wss://ws-rpc-bg1.chainfusion.org</a></td></tr></tbody></table>

## Bridgenet 2

<table><thead><tr><th width="390">Name</th><th>URL</th></tr></thead><tbody><tr><td>Chain ID</td><td><code>9992</code></td></tr><tr><td>Explorer</td><td><a href="https://explorer-bg2.chainfusion.org">https://explorer-bg2.chainfusion.org</a></td></tr><tr><td>HTTPS RPC API</td><td><a href="https://rpc-bg2.chainfusion.org">https://rpc-bg2.chainfusion.org</a></td></tr><tr><td>WebSocket RPC API</td><td><a href="wss://ws-rpc-bg2.chainfusion.org">wss://ws-rpc-bg2.chainfusion.org</a></td></tr></tbody></table>

## Bridgenet 3

<table><thead><tr><th width="390">Name</th><th>URL</th></tr></thead><tbody><tr><td>Chain ID</td><td><code>9993</code></td></tr><tr><td>Explorer</td><td><a href="https://explorer-bg3.chainfusion.org">https://explorer-bg3.chainfusion.org</a></td></tr><tr><td>HTTPS RPC API</td><td><a href="https://rpc-bg3.chainfusion.org">https://rpc-bg3.chainfusion.org</a></td></tr><tr><td>WebSocket RPC API</td><td><a href="wss://ws-rpc-bg3.chainfusion.org">wss://ws-rpc-bg3.chainfusion.org</a></td></tr></tbody></table>
